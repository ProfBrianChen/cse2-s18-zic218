// Name: Zichen Chen
// Course: CSE002
// Project name: HW02

public class Arithmetic {
  
    public static void main(String[] args) {
      
      //Number of pairs of pants
      int numPants = 3;
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of sweatshirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
      //cost per belt
      double beltCost = 33.99;
  
      //the tax rate
      double paSalesTax = 0.06;
      
      
      //Total cost of each kind of item.
      
      double totalCostOfPants = numPants * pantsPrice;   //total cost of pants
      
      double totalCostOfSweatshirts = numShirts * shirtPrice;  //total cost of shirts
      
      double totalCostOfBelts = numBelts * beltCost;   //total cost of belts
      
      
      //Sales tax charged buying all of each kind of item.
      
      double SalesTaxOnPants = numPants * pantsPrice * paSalesTax;   //sales tax on pants
      
      double SalesTaxOnSweatshirts = numShirts * shirtPrice * paSalesTax;   //sales tax on shirts
      
      double SalesTaxOnBelts = numBelts * beltCost * paSalesTax;   //sales tax on belts
      
      int ValueOfSalesTaxOnPants = (int) (SalesTaxOnPants * 100);
        
      int ValueOfSalesTaxOnSweatshirts = (int) (SalesTaxOnSweatshirts * 100);
      
      int ValueOfSalesTaxOnBelts = (int) (SalesTaxOnBelts * 100);
      
      double FinalSalesTaxOnPants = ValueOfSalesTaxOnPants * 0.01;
         
      double FinalSalesTaxOnShirts = ValueOfSalesTaxOnSweatshirts * 0.01;
      
      double FinalSalesTaxOnBelts = ValueOfSalesTaxOnBelts / 100.0;
      
      
     
      
      
      //Total cost of purchases before tax.
      
      double TotalCostOfPurchases = totalCostOfPants + totalCostOfSweatshirts + totalCostOfBelts;   //total cost of purchases before tax 
      
      
      //Total sales tax.
      
      double TotalSalesTax = FinalSalesTaxOnPants + FinalSalesTaxOnShirts + FinalSalesTaxOnBelts;   //total sales tax.
      
      
      //Total paid for the transaction. Sales tax included.
      
      double TotalPaid = TotalCostOfPurchases + TotalSalesTax;   //total paid for the transaction including sales tax.
      
      
      System.out.println("The total cost of pants is "+ totalCostOfPants);
      System.out.println("The total cost of shirts is "+ totalCostOfSweatshirts);
      System.out.println("The total cost of belts is "+ totalCostOfBelts);
      
      System.out.println("The sales tax on pants is "+ FinalSalesTaxOnPants);
      System.out.println("The sales tax on shirts is "+ FinalSalesTaxOnShirts);
      System.out.println("The sales tax on belts is "+ FinalSalesTaxOnBelts);
      
      System.out.println("The total cost of purchases is "+ TotalCostOfPurchases);
      
      System.out.println("The total sales tax is "+ TotalSalesTax);
      
      System.out.println("The total paid for the transaction including sales tax is "+ TotalPaid);
      
      
      
    }  
  
  
  
  
  
  
  
  
}