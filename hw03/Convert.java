// Name: Zichen Chen
// Course: CSE002
// Project name: HW03

import java.util.Scanner; // import

public class Convert {
  public static void main(String[] args) {
    
    // Start by creating a Scanner object and a declaration
    Scanner myScanner;
    myScanner = new Scanner(System.in);
    
    // Prompts the user to enter the number of affected area in acres
    System.out.println("Enter the affected area in acres:");
    double Area = myScanner.nextDouble();
    
    // Prompts the user to enter the number of rainfall in inches
    System.out.println("Enter the rainfall in the affected area:");
    double Rainfall = myScanner.nextDouble();
    
    // Convert, compute and display the final volume of water
    double CubicMiles = Area * 0.0015624989 * Rainfall * 0.0000157828;
    System.out.println(CubicMiles + " cubic miles"); 
    
   
    
    }

  
}
 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
