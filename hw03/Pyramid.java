// Name: Zichen Chen
// Course: CSE002
// Project name: HW03

import java.util.Scanner; // import

public class Pyramid {
  public static void main(String[] args) {
    
    // Start by creating a Scanner object and a declaration
    Scanner myScanner;
    myScanner = new Scanner(System.in);
    
    // Prompts the user to enter the number of length of square side
    System.out.println("The square side of the pyramid is (input length):");
    double Length = myScanner.nextDouble();
    
    // Prompts the user to enter the number of height of pyramid
    System.out.println("The height of the pyramid is (input height):");
    double Height = myScanner.nextDouble();
    
    // Compute and display the final volume of the pyramid
    double Volume = (Length * Length * Height) / 3 ;
    System.out.println("The volume inside the pyramid is: "+ Volume); 
    
   
    
    }

  
}
 