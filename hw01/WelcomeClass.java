// Name: Zichen Chen
// Course: CSE002

public class WelcomeClass {
    // main method is mandatory for each Java programme
    public static void main(String[] args) {
      
      System.out.println("   -----------");
      System.out.println("   | WELCOME |");
      System.out.println("   -----------");
      System.out.println("   ^  ^  ^  ^  ^  ^");
      System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println(" <-Z--I--C--2--1--8->");
      System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("   v  v  v  v  v  v ");
      
      
      
    }
  
  
 }